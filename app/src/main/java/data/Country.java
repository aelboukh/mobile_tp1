package data;

public class Country {
    private String name;
    private String capital;
    private String imgFile;
    private String language;
    private String currency;
    private int population;
    private int area;

    public Country(String name, String capital, String imgFile, String language, String currency, int population, int area) {
        this.name = name;
        this.capital = capital;
        this.imgFile = imgFile;
        this.language = language;
        this.currency = currency;
        this.population = population;
        this.area = area;
    }

    public String getName() {
        return name;
    }

    public void setName(String capital) {
        this.name = name;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    public String getImgUri() {
        return "@drawable/ic_"+imgFile+"_320px";
    }

    public void setImgFile(String imgFile) {
        this.imgFile = imgFile;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public int getPopulation() {
        return population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }

    public int getArea() {
        return area;
    }

    public void setArea(int area) {
        this.area = area;
    }
}
