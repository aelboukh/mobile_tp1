package fr.uavignon.tp1;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import data.Country;
import data.CountryList;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

    private ArrayList<String> countryNames = new ArrayList<String>();
    private Context c;
    public RecyclerAdapter(Context c, ArrayList<String> countryNames) {
        this.countryNames = countryNames;
        this.c = c;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.card_layout, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

        // Name of the country
        String name = countryNames.get(i);
        Country country = CountryList.getCountry(name);
        viewHolder.countryName.setText(name);
        viewHolder.countryCapital.setText(country.getCapital());
        String uri = country.getImgUri();
        viewHolder.countryImage.setImageDrawable(c.getResources().getDrawable(c.getResources().getIdentifier(uri, null, c.getPackageName())));
    }

    @Override
    public int getItemCount() {
        return countryNames.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView countryImage;
        TextView countryName;
        TextView countryCapital;

        ViewHolder(View itemView) {
            super(itemView);
            countryImage = itemView.findViewById(R.id.item_image);
            countryName = itemView.findViewById(R.id.item_title);
            countryCapital = itemView.findViewById(R.id.item_detail);


            int position = getAdapterPosition();

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    int position = getAdapterPosition();
                    /* Snackbar.make(v, "Click detected on chapter " + (position+1),
                        Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();

                     */
                    //// Implementation with bundle
                    // Bundle bundle = new Bundle();
                    // bundle.putInt("numChapter", position);
                    // Navigation.findNavController(v).navigate(R.id.action_FirstFragment_to_SecondFragment, bundle);

                    ListFragmentDirections.ActionFirstFragmentToSecondFragment action = ListFragmentDirections.actionFirstFragmentToSecondFragment();
                    action.setCountryId(position);
                    Navigation.findNavController(v).navigate(action);
                }
            });

        }
    }

}